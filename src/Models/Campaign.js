const Decimal = require('decimal.js')
const moment = require('moment')

const RP_Venue = require('./Venue').RP_Venue

const { CAMPAIGN_FUNDING_TYPE, CAMPAIGN_STATUS } = require('../enums')


class Campaign {
  constructor(campaign) {
    //STATIC PROPERTIES
    this.campaign          = campaign
    this.id                = campaign ? campaign.id : null
    this.is_featured       = campaign ? campaign.is_featured : false
    this.picture           = campaign ? campaign.picture : null
    this.title             = campaign ? campaign.title : ''
    this.bands             = campaign ? campaign.bands : []
    this.organizations     = campaign ? campaign.organizations : []
    this.description       = campaign ? campaign.description : ''
    this.tickets_sold      = campaign ? campaign.tickets_sold : 0
    this.total_pledged     = campaign ? campaign.total_pledged : 0
    this.service_fee       = campaign ? campaign.service_fee : 0.00
    this.timeslot          = campaign ? campaign.timeslot : null
    this.is_only_tickets   = campaign ? campaign.is_only_tickets : false
    this.is_venue_approved = campaign ? campaign.is_venue_approved : null
    this.funding_end       = campaign ? campaign.funding_end : new Date()
    this.event_start       = this.timeslot ? this.timeslot.start_time : new Date()
    this.venue             = this.timeslot ? this.timeslot.venue : null

    this._Venue            = Number.isInteger(this.venue) ? RP_Venue({id:this.venue}) : RP_Venue(this.venue)
  }

  //COMPUTED PROPERTIES
  progress() {
    if(!this.campaign) return 0
    if(!this.is_venue_approved) return 0

    const timeslot      = this.campaign && this.campaign.timeslot || null
    const goal_count    = new Decimal(timeslot && timeslot.min_headcount || 0)
    const goal_amount   = new Decimal(timeslot && timeslot.asking_price || 0)
    const tickets_sold  = new Decimal(this.campaign && this.campaign.tickets_sold || 0)
    const total_pledged = new Decimal(this.campaign && this.campaign.total_pledged || 0)

    const funding_progress   = goal_amount == 0 ? 100 : parseInt(total_pledged.div(goal_amount).mul(new Decimal('100')))
    const headcount_progress =  goal_count == 0 ? 100 : parseInt(tickets_sold.div(goal_count).mul(new Decimal('100')))

    switch (this.campaign.funding_type) {
      case CAMPAIGN_FUNDING_TYPE.GOAL_AMOUNT:
        return funding_progress

      case CAMPAIGN_FUNDING_TYPE.GOAL_COUNT:
        return headcount_progress

      case CAMPAIGN_FUNDING_TYPE.HYBRID:
        return this.campaign.is_successful
               ? Math.max(funding_progress,headcount_progress)
               : Math.min(funding_progress,headcount_progress)
    }
  }

  //Returns whether or not a given show is corwdfunded.
  isCrowdfunded() {
    if(this.campaign == null) return false

    return !(this.isTraditional() || this.campaign.is_only_tickets)
  }

  //Returns whether or not a show has reached it's goal.
  isSuccess() {
    return (this.progress() >= 100)
  }

  //Returns the show's current status.
  status() {
    if(this.campaign == null) return null

    const isApproved = this.is_venue_approved
    const isPending  = (this.is_venue_approved == null)
    const isSuccess  = this.isSuccess()
    const isSelling  = this.isSelling()

    return isPending
           ? isSelling
             ? CAMPAIGN_STATUS.PENDING_APPROVAL
             : CAMPAIGN_STATUS.PENDING_FOREVER
           : isApproved
             ? isSuccess
               ? isSelling
                 ? CAMPAIGN_STATUS.SUCCESSFUL
                 : CAMPAIGN_STATUS.FINISHED
               : isSelling
                 ? CAMPAIGN_STATUS.IN_PROGRESS
                 : CAMPAIGN_STATUS.FAILED
             : CAMPAIGN_STATUS.REJECTED
  }

  //Returns the amount of time remaining to purchase tickets. (string)
  timeRemaining() {
    if(!this.campaign) return null

    const start       = moment()
    const end         = moment(this.funding_end)
    const diffDays    = end.diff(start, 'days')
    const diffHours   = end.diff(start, 'hours')
    const diffMinutes = end.diff(start, 'minutes')
    const diffSeconds = end.diff(start, 'seconds')

    if (diffDays == 1) return `${diffDays} Day`
    if (diffDays > 1) return `${diffDays} Days`

    if (diffHours === 1) return `${diffHours} Hour`
    if (diffHours > 1) return `${diffHours} Hours`

    if (diffMinutes === 1) return `${diffMinutes} Minute`
    if (diffMinutes > 1) return `${diffMinutes} Minutes`

    if (diffSeconds > 0) return `${diffSeconds} Seconds`

    return `Completed`
  }

  //Returns whether or not tickets are available for purchase.
  isSelling(){
    if(!this.campaign) return false

    return moment(this.funding_end).isAfter(moment(Date.now()))
  }

  //Returns whether or not a show has all zero goals - making it a non-crowdfunded show.
  isTraditional(){
    if(!this.campaign) return false

    const timeslot      = this.campaign.timeslot || null
    const asking_price  = timeslot && timeslot.asking_price || 0
    const min_headcount = timeslot && timeslot.min_headcount || 0

    return (asking_price == 0 && min_headcount == 0)
  }

  //Returns a shortened version of the show description.
  shortDescription() {
    if(!this.campaign) return false

    return this.description.substring(0, 150) + (this.description.length > 150 ? '...' : '')
  }

  //Returns whether or not a user is a member of one of the performing acts.
  userInAct(user) {
    if(!user) return false

    const acts    = this.campaign && this.campaign.bands || []
    const user_id = user && user.id || null

    let in_campaign = false
    acts.forEach(campaignBand => {
      (campaignBand.band.artists || []).forEach(artist => {
        //Sometimes we get ids instead of objects.
        Number.isInteger(artist.user)
        ? artist.user == user_id
          ? in_campaign = true
          : null
        : artist.user && (artist.user.id == user_id)
          ? in_campaign = true
          : null
      })
    })
    return in_campaign
  }

  //Returns whether or not a user is the show's creator.
  userIsCreator(user) {
    if(!user) return false
    if(!this.campaign) return false

    return (user.id == this.campaign.created_by)
  }

  //Returns whether or not a user is a manager for the venue.
  userIsVenueManager(user){
    if(!user) return false
    if(!this.campaign) return false

    const timeslot = this.campaign.timeslot || null
    const venue    = timeslot && timeslot.venue || null
    const managers = venue && venue.managers || []

    let isManager = false
    managers.forEach(manager => {
      if(manager == user.id){
        isManager = true
      } 
      else if(manager.id == user.id){
        isManager = true
      } 
      else if(manager.manager && manager.manager.id == user.id){
        isManager = true
      } 
    })
    return isManager
  }  

  //Returns whether or not a user is a member of an organization.
  userIsOrganizationManager(user){
    if(!user) return false
    if(!this.campaign) return false
      
    let isManager = false
    this.organizations.forEach(campaign_organization => {
      const managers = campaign_organization.organization.managers || []

      managers.forEach(manager => {
        if(manager == user.id){
          isManager = true
        } 
        else if(manager.id == user.id){
          isManager = true
        } 
        else if(manager.manager && manager.manager.id == user.id){
          isManager = true
        } 
      })
    })

    return isManager
  }  

  //Returns whether or not the user has an open invitation to the show.
  hasOpenInviteToUserAct(user){
    if(!user) return false

    const acts    = this.campaign && this.campaign.bands || []
    const user_id = user && user.id || null

    let has_open_invite = false
    acts.forEach(campaignBand => {
      campaignBand.is_accepted == null
      ? (campaignBand.band.artists || []).forEach(artist => {
          //Sometimes we get ids instead of objects.
          Number.isInteger(artist.user)
          ? artist.user == user_id
            ? has_open_invite = true
            : null
          : artist.user && (artist.user.id == user_id)
            ? has_open_invite = true
            : null
        })
      : null
    })
    return has_open_invite
  }

  //Returns the first open invite to the show, if one of the user's acts was invited.
  firstOpenInviteToUserAct(user){
    if(!user) return null

    const acts    = this.campaign && this.campaign.bands || []
    const user_id = user && user.id || null

    let open_invite = null
    acts.forEach(campaignBand => {
      if (campaignBand.is_accepted === null) {
        (campaignBand.band.artists || []).forEach(artist => {
          //Sometimes we get ids instead of objects.
          Number.isInteger(artist.user)
          ? artist.user == user_id
            ? open_invite = campaignBand
            : null
          : artist.user && (artist.user.id == user_id)
            ? open_invite = campaignBand
            : null
        })
      }
    })
    return open_invite
  }

  //Returns the cheapest ticket value. 
  //(String by default, pass true for a numberic value)
  cheapestTicket(force_number=false){
    if(!this.campaign) return 0.00

    var price = Number.MAX_SAFE_INTEGER
    if(this.campaign.purchase_options){
      this.campaign.purchase_options.forEach(item => {
        if(item.is_ticket){price = Math.min(item.price,price)}
      })
    }
    return force_number
            ? price == Number.MAX_SAFE_INTEGER 
              ? 0 
              : price.toFixed(2)
            : price == Number.MAX_SAFE_INTEGER 
              ? "No advance tickets." 
              : price.toFixed(2)
  }

  //Returns the Band object of the show's headliner.
  //If there are more than one headliner then the first one will be returned.
  headliner() {
    if(!this.campaign) return null

    const acts = this.campaign.bands || []
    const all_headliners = acts.filter(act => act.is_headliner)

    return (all_headliners.length > 0 ? all_headliners[0].band : null)
  }

  //Returns all supporting acts.
  supportingActs(){
    if(!this.campaign) return false

    return this.bands.reduce((acts, curr) => {
      if(!curr.is_headliner && curr.is_accepted !== false ){
        return acts.concat(curr.band)
      }else{
        return acts
      }}, [])
  }
}

function RP_Campaign(campaign) {
  return new Campaign(campaign)
}

module.exports = {
  RP_Campaign
}