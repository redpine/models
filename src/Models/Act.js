class Act {
  constructor(act){
    this.act         = act
    this.id          = act ? act.id : null
    this.name        = act ? act.name : ''
    this.short_bio   = act ? act.short_bio : '' 
    this.picture     = act ? act.picture : null 
    this.is_featured = act ? act.is_featured : false

    this.genre       = act ? act.genre : null 
    this.genre_name  = this.genre ? this.genre.name : '' 

    this.website     = act ? act.website : ''
    this.facebook    = act ? act.facebook : ''
    this.twitter     = act ? act.twitter : ''
    this.instagram   = act ? act.instagram : ''
    this.spotify     = act ? act.spotify : ''
    this.soundcloud  = act ? act.soundcloud : ''
    this.bandcamp    = act ? act.bandcamp : ''
    this.youtube     = act ? act.youtube : ''
  }

  hasSocialLink() {
  	if(!this.act) return false

  	return (this.website || this.facebook || this.twitter || this.instagram || this.spotify || this.soundcloud || this.bandcamp || this.youtube)
  }
}

function RP_Act(venue) {
  return new Act(venue)
}

module.exports = {
  RP_Act
}