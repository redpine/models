
class Organization {
	constructor(organization){
     //STATIC PROPERTIES
		this.organization     = organization
    this.id               = organization ? organization.id : null
    this.title            = organization ? organization.title : ''
    this.picture          = organization ? organization.picture : null
    this.description      = organization ? organization.description : ''

    this.location         = organization ? organization.location : null 
    this.address          = organization ? organization.address : '' 
    this.postal_code      = organization ? (organization.postal_code && organization.postal_code.toUpperCase()) : '' 
    this.city             = organization ? organization.city : null 
    this.province         = this.city ? this.city.province : null 
    this.country          = this.province ? this.province.country : null 

    this.city_name        = this.city ? this.city.name : '' 
    this.province_name    = this.province ? this.province.name : '' 
    this.country_name     = this.country ? this.country.name : '' 
    this.address_string   = `${this.address}. ${this.city_name}, ${this.province_name}. ${this.country_name}. ${this.postal_code}`
	}
}

function RP_Organization(organization) {
  return new Organization(organization)
}

module.exports = {
    RP_Organization
}