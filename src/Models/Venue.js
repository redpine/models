const moment = require('moment')

class Venue {
	constructor(venue){
     //STATIC PROPERTIES
		this.venue            = venue
    this.id               = venue ? venue.id : null
    this.title            = venue ? venue.title : ''
    this.picture          = venue ? venue.picture : null
    this.description      = venue ? venue.description : ''
    this.currency         = venue ? venue.currency : ''
    this.capacity         = venue ? venue.capacity : null

    this.location         = venue ? venue.location : null 
    this.address          = venue ? venue.address : '' 
    this.postal_code      = venue ? (venue.postal_code && venue.postal_code.toUpperCase()) : '' 
    this.city             = venue ? venue.city : null 
    this.province         = this.city ? this.city.province : null 
    this.country          = this.province ? this.province.country : null 

    this.city_name        = this.city ? this.city.name : '' 
    this.province_name    = this.province ? this.province.name : '' 
    this.country_name     = this.country ? this.country.name : '' 
    this.address_string   = `${this.address}. ${this.city_name}, ${this.province_name}. ${this.country_name}. ${this.postal_code}`

    this.primary_genre    = venue ? venue.primary_genre : null
    this.secondary_genre  = venue ? venue.secondary_genre : null

    this.genre_string     = this.primary_genre && this.secondary_genre
                            ? `${this.primary_genre.name} / ${this.secondary_genre.name}`
                            : this.primary_genre
                              ? this.primary_genre.name
                              : 'No preferred genre.'

    this.reviews_by_bands = venue ? venue.reviews_by_bands : []

    this.default_fee_weekday = venue ? venue.default_fee_weekday : 0.00
    this.default_fee_weekend = venue ? venue.default_fee_weekend : 0.00

    this.default_headcount_weekday = venue ? venue.default_headcount_weekday : 0
    this.default_headcount_weekend = venue ? venue.default_headcount_weekend : 0

    this.is_non_redpine_default = venue ? venue.is_non_redpine_default : false
	}

  //COMPUTED PROPERTIES
  // Convert API utc date/time into the venue's local timezone.
	localTimeFromUTC(utc_time) {
		if(!this.venue) return moment(null)

    return moment(utc_time).add(this.venue.utc_offset, 'hours')
  }

  // Convert browser-generated date/time into the venue's local timezone
  localTimeFromBrowser(browser_time) {
  	if(!this.venue) return moment(null)

    const offset  = moment(browser_time).utcOffset() / 60
    const totalOffset = offset - this.venue.utc_offset

    return moment.utc(browser_time).add(totalOffset, 'hours').toISOString().replace('.000Z', '')
  }

  hasReviews() {
    if(!this.venue) return false

    return (this.reviews_by_bands.length > 0)
  }
}

function RP_Venue(venue) {
  return new Venue(venue)
}

module.exports = {
  RP_Venue
}