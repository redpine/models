var path = require('path');

var config = {
  context: path.resolve(__dirname + '/src'),
  entry: ['./index.js',],
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname + '/dist'),
    library: 'default',
    libraryTarget: 'commonjs'
  },
};

module.exports = config;
